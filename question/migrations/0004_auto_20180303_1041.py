# Generated by Django 2.0.2 on 2018-03-03 10:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('question', '0003_auto_20180303_1038'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ['order'], 'verbose_name': 'Вопрос', 'verbose_name_plural': 'Вопроы'},
        ),
        migrations.AddField(
            model_name='question',
            name='order',
            field=models.IntegerField(default=500, help_text='Чем больше число, тем ниже вопрос', verbose_name='Сортировка'),
        ),
        migrations.AlterField(
            model_name='question',
            name='timestamp',
            field=models.DateTimeField(default=datetime.datetime(2018, 3, 3, 10, 41, 55, 335818), verbose_name='Дата создания'),
        ),
    ]
