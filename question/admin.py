from django.contrib import admin
from question.models import *


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Основное', {'fields': ['text', 'is_public', 'is_all_answers']}),
        ('Дата', {'fields': ['answer_time'], }),

    ]
    inlines = [AnswerInline]
    list_display = ('text','is_all_answers', 'is_public')
    #filter_horizontal = ('test', 'is_public')
    list_filter = ('is_public', 'is_all_answers','test', )
class TestProcessAdmin(admin.ModelAdmin):
    actions = [start_test]
    list_display = ('test', 'current_question','started', 'finished')
class TestResultAdmin(admin.ModelAdmin):
    list_display = ('test', 'user', 'true_answers', 'false_answers', 'empty_answers')

class TestGroupAdmin(admin.ModelAdmin):
    filter_horizontal = ('tests', )

class TestAdmin(admin.ModelAdmin):
    filter_horizontal = ('questions', )
# Register your models here.
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer)
admin.site.register(Test, TestAdmin)
admin.site.register(TestProcess, TestProcessAdmin)
admin.site.register(TestResult, TestResultAdmin)
admin.site.register(TestGroup, TestGroupAdmin)