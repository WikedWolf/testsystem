from django.shortcuts import render, redirect
from django.http import JsonResponse
from question.models import *
import traceback
from django.contrib.auth.decorators import login_required
from datetime import datetime as dt
def checkInTest(request):
    try:
        test = TestProcess.objects.get(users=request.user, finished=False)
        if test.started:
            url = '/test/%s/process/%s' % (test.id, test.current_question.id)
            return url
        else:
            url = '/test/%s/waiting' % test.id
            return url
    except:
        print(traceback.format_exc())
        return False

@login_required(login_url='/auth/login/')
def homepage(request):
    redir = checkInTest(request)
    if redir:
        return redirect(redir)
    groups = TestGroup.objects.filter(is_public=True)[:5]
    context = {
        'groups': groups,
    }
    template = 'test/homepage.html'
    return render(request, template, context)



@login_required(login_url='/auth/login/')
def listview(request, test_group_id = None, test_id = None):
    redir = checkInTest(request)
    if redir:
        return redirect(redir)
    groups = None
    group = None
    test = None

    if test_group_id is None:
        page_title = "Выберите группу тестов"
        groups = TestGroup.objects.filter(is_public=True)
    if test_group_id is not None:
        page_title = "Выберите  тест"
        group = TestGroup.objects.get(id = test_group_id)

    if test_id is not None:
        test = Test.objects.get(id = test_id)
        page_title = 'Тест "%s"' % test.title

    context = {
        'test_group_id': test_group_id,
        'test_id': test_id,
        'page_title': page_title,
        'groups': groups,
        'group': group,
        'test': test
    }
    template = 'test/list.html'
    return render(request, template, context)



@login_required(login_url='/auth/login/')
def join(request, test_id = None):
    redir = checkInTest(request)
    if redir:
        return redirect(redir)
    if test_id is not None:
        test = Test.objects.get(id=test_id)
        testProcess, created = TestProcess.objects.get_or_create(
            test = test,
            started = False,
            finished = False
        )

        testProcess.save()
        testProcess.users.add(request.user)
        testProcess.save()






        return redirect('waiting', test_id=testProcess.id)



@login_required(login_url='/auth/login/')
def waiting(request, test_id):
    test = TestProcess.objects.get(id=test_id)
    page_title = "Ожидаем начала тестирования"
    context = {
        'test_id': test_id,
        'page_title': page_title,
        'test': test
    }
    template = "test/wait.html"
    return render(request, template, context)


@login_required(login_url='/auth/login/')
def leave(request, test_id = None):
    test = TestProcess.objects.get(id=test_id)
    test.users.remove(request.user)
    test.save()
    return redirect('/list/')


@login_required(login_url='/auth/login/')
def update(request, test_id=None):
    test = TestProcess.objects.get(id=test_id)
    data = {
        'members': test.users.all().count(),
        'start': test.started,

    }
    if test.started:
        data['url'] = '/test/%s/start' % test.id
    return JsonResponse(data)


@login_required(login_url='/auth/login/')
def start(request, test_id):
    test = TestProcess.objects.get(id=test_id)
    testResult = TestResult(user=request.user, test = test)
    testResult.save()
    questions = list(test.test.questions.all())
    test.current_question = questions[0]
    test.time_question = dt.now()
    test.save()
    url = '/test/%s/process/%s' % (test.id, test.current_question.id)
    return redirect(url)




@login_required(login_url='/auth/login/')
def process(request, test_id, question_id=None):
    test = TestProcess.objects.get(id=test_id)
    context = {
        'page_title': 'Отвечайте на вопрос',
        'test': test,
    }
    template = 'test/process.html'
    return render(request, template, context)


@login_required(login_url='/auth/login/')
def answer(request, test_id, question_id = None):
    answer = request.POST.get('answer')
    if not request.POST.get('answer'):
        return redirect('/test/%s/process/%s' % (test_id, question_id))
    test = TestProcess.objects.get(id=test_id)
    is_true = True
    if test.current_question.is_all_answers:
        answer = request.POST.getlist('answer')
        print(answer)
        print(dict(request.GET))
        for a in answer:
            answerObject = Answer.objects.get(id = a)

            if not answerObject.is_true:
                is_true = False
        if is_true:
            true_answers = test.current_question.answer_set.filter(is_true=True)
            if not true_answers.count() == answer.__len__():
                is_true  = False
    elif not test.current_question.is_all_answers:

        answerObject = Answer.objects.get(id=answer)
        if not answerObject.is_true:
            is_true = False
    print(is_true)

    result = TestResult.objects.get(user=request.user, test = test)
    if is_true:
        result.true_answers += 1
    else:
        result.false_answers += 1

    result.save()
    if is_true or test.users.count() == 1:
        questions = list(test.test.questions.all())
        is_next = False
        for q in questions:
            if is_next:
                test.current_question = q
                test.time_question = dt.now()
                test.save()
                url = '/test/%s/process/%s' % (test.id, test.current_question.id)
                return redirect(url)
            if q.id == question_id:
                is_next = True
    else:
        url = '/test/%s/waitmembers/%s' % (test.id, test.current_question.id)
        return redirect(url)
    test.finished = True
    test.save()
    url = '/test/%s/result' % test.id
    return redirect(url)


@login_required(login_url='/auth/login/')
def wait_members(request, test_id=None, question_id=None):
    test = TestProcess.objects.get(id=test_id)
    page_title = "Ожидаем ответа остальных участников"
    context = {
        'test':test,
        'page_title': page_title
    }
    template = 'test/waitmembers.html'
    return render(request, template, context)


@login_required(login_url='/auth/login/')
def result(request, test_id):
    result = TestResult.objects.get(test__id=test_id, user = request.user)
    page_title = 'Тест завершен'
    context = {
        'page_title': page_title,
        'result': result
    }
    template = 'test/result.html'
    return render(request, template, context)

@login_required(login_url='/auth/login/')
def currentQuestion(request, test_id, question_id = None):
    test = TestProcess.objects.get(id=test_id)
    url = '/test/%s/process/%s' % (test.id, test.current_question.id)

    if test.current_question.id == int(question_id) and not test.timeout() and not test.finished:
        data = {
            'success': True,
        }
    else:
        if test.finished:
            url = url = '/test/%s/result' % test.id
        if test.timeout():
            message = "Время истекло, вы будете перенаправлены на следующий вопрос"
        else:
            message = 'Вас опередили с ответом, вы будете перенаправлены на следующий вопрос'
        data = {
            'success': False,
            'message': message,
            'url': url
        }
    return JsonResponse(data)


@login_required(login_url='/auth/login/')
def timeout(request, test_id=None, question_id=None):
    test = TestProcess.objects.get(id=test_id)
    questions = list(test.test.questions.all())
    is_next = False
    for q in questions:
        if is_next:
            test.current_question = q
            test.time_question = dt.now()
            test.save()
            url = '/test/%s/process/%s' % (test.id, test.current_question.id)
            return JsonResponse({'url': url})
        if q.id == question_id:
            is_next = True

    test.finished = True
    test.save()
    url = '/test/%s/result' % test.id
    return JsonResponse({'url': url})


