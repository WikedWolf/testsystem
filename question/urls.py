from django.urls import path
from question.views import *
urlpatterns = [
    path('', homepage),
    path('list/', listview, name='question_list'),
    path('list/<int:test_group_id>/tests/', listview,  name='list_tests'),
    path('list/<int:test_group_id>/tests/<int:test_id>', listview,  name='test_view'),
    path('test/<int:test_id>/join', join, name="join_to_test"),
    path('test/<int:test_id>/leave', leave, name="leave_test"),
    path('test/<int:test_id>/waiting', waiting, name="waiting"),
    path('test/<int:test_id>/update', update, name="check_test_status"),
    path('test/<int:test_id>/question/<int:question_id>', currentQuestion, name="check_question"),
    path('test/<int:test_id>/start', start, name="start_test"),
    path('test/<int:test_id>/process/<int:question_id>', process, name="process_test"),
    path('test/<int:test_id>/answer/<int:question_id>', answer, name="answer_test"),
    path('test/<int:test_id>/result', result, name="result_test"),
    path('test/<int:test_id>/timeout/<int:question_id>', timeout, name="timeout_test"),
    path('test/<int:test_id>/waitmembers/<int:question_id>', wait_members, name="twaitmembers_test"),

]