from django.db import models
from datetime import datetime as dt, timedelta as td
from django.contrib.auth.models import User
# Create your models here.
import pytz



def start_test(modeladmin, request, queryset):
    queryset.update(started=True)
start_test.short_description = "Начать тестирование"

class Question(models.Model):
    text = models.TextField(verbose_name="Текст вопроса")
    answer_time = models.TimeField(verbose_name="Время на ответ", default='00:05:00')
    is_all_answers = models.BooleanField(verbose_name="Несколько ответов", default=False, help_text="Если флаг установлен, нужно выбрать все правильные ответы для зачета")
    is_public = models.BooleanField(verbose_name="Опубликоан", default=True)
    order = models.IntegerField(verbose_name="Сортировка", default=500, help_text="Чем больше число, тем ниже вопрос")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name="Вопрос"
        verbose_name_plural="Вопроы"
        ordering = ['order',]

    def randAnswers(self):
        return self.answer_set.all().order_by('?')

class Answer(models.Model):
    text = models.CharField(verbose_name='Текст ответа', max_length=255)
    is_true = models.BooleanField(verbose_name="Правильный", default=False)
    question = models.ForeignKey(Question, verbose_name='Вопрос', on_delete=models.CASCADE)
    order = models.IntegerField(verbose_name="Сортировка", default=500, help_text="Чем больше число, тем ниже ответ")

    def __str__(self):
        return self.text

    class Meta:
        verbose_name="Ответ"
        verbose_name_plural="Ответы"
        ordering = ['order', ]



class Test(models.Model):
    title = models.CharField(verbose_name="Название теста", max_length=255)
    questions = models.ManyToManyField(Question, verbose_name='Вопросы')
    is_public = models.BooleanField(verbose_name="Опубликоан", default=False)
    order = models.IntegerField(verbose_name="Сортировка", default=500, help_text="Чем больше число, тем ниже тест")
    def __str__(self):
        return self.title

    class Meta:
        verbose_name="Тест"
        verbose_name_plural="Тесты"
        ordering = ['order', ]


class TestProcess(models.Model):
    test = models.ForeignKey(Test, verbose_name="Тест", on_delete=models.CASCADE)
    users = models.ManyToManyField(User, verbose_name='Пользователи', null=True)
    timestamp = models.DateTimeField(verbose_name='Дата создания', default=dt.now())
    started = models.BooleanField(verbose_name="Тестирование началось", default=False)
    finished = models.BooleanField(verbose_name="Тестирование закончилось", default=False)
    current_question = models.ForeignKey(Question, verbose_name="Текущий вопрос", on_delete=models.DO_NOTHING, null=True, blank=True)
    time_question = models.DateTimeField(verbose_name="Время постановки вопроса", null=True, blank=True)
    def __str__(self):
        return self.test.title


    def timeout(self):
        utc = pytz.UTC
        expireTime  = self.time_question + td(minutes = self.current_question.answer_time.minute, hours=self.current_question.answer_time.hour, seconds=self.current_question.answer_time.second)
        if utc.localize(dt.now()) <= expireTime:
            return False
        else:
            return True

    class Meta:
        verbose_name="Тестирование"
        verbose_name_plural="Тестирования"

class TestResult(models.Model):
    test = models.ForeignKey(TestProcess, verbose_name="Тестирование", on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name="Пользователь", on_delete=models.DO_NOTHING)
    time = models.TimeField(verbose_name="Время прохождения теста", null=True, blank=True)
    timestamp = models.DateTimeField(verbose_name='Дата прохождения', default=dt.now())
    true_answers = models.IntegerField(verbose_name="Правильных ответов", null=True, default=0)
    false_answers = models.IntegerField(verbose_name="Не правильныз ответов", null=True, default=0)
    empty_answers = models.IntegerField(verbose_name="Не успел ответить", null=True, default=0)
    def __str__(self):
        return "%s-%s" % (self.user.username, self.test.test.title)

    class Meta:
        verbose_name="Результат"
        verbose_name_plural="Результаты"

    def getPercentTrue(self):
        persent = round(100 / self.test.test.questions.all().count() * self.true_answers, 2)
        return  persent

    def getEmpty(self):
        self.empty_answers = self.test.test.questions.all().count() - (self.true_answers + self.false_answers)
        self.save()
        return self.empty_answers

class TestGroup(models.Model):
    title = models.CharField(verbose_name="Название набора", max_length=255)
    tests = models.ManyToManyField(Test, verbose_name='Вопросы')
    is_public = models.BooleanField(verbose_name="Опубликоан", default=False)
    order = models.IntegerField(verbose_name="Сортировка", default=500, help_text="Чем больше число, тем ниже набор")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Набор тестов"
        verbose_name_plural = "Наборы тестов"
        ordering = ['order', ]
