from django.urls import path
from authenticate.views import *
urlpatterns = [
    path('login/', LoginFormView.as_view(), name="login"),
    path('registration/', RegisterFormView.as_view(), name='register'),
    path('logout/', LogoutView.as_view(), name='logout'),

]