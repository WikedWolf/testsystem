from django.shortcuts import render

from django import forms
from django.contrib.auth.models import User


from django.contrib.auth.forms import AuthenticationForm

from django.contrib.auth import login

from django.views.generic.edit import FormView
from django.contrib.auth.forms import UserCreationForm

from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.contrib.auth import logout



class RegisterFormView(FormView):
    form_class = UserCreationForm

    success_url = "/login/"


    template_name = "base/register.html"

    def form_valid(self, form):

        form.save()

        return super(RegisterFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm

    template_name = "base/login.html"

    success_url = "/"

    def form_valid(self, form):

        self.user = form.get_user()

        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)



class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect("/")