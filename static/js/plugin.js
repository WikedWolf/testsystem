$.fn.checkMembers = function(){
    var testBlock = $(this);
    var url = testBlock.attr('check-url')
    setInterval(function () {
        $.ajax({
      url: url,
      success: (function (data) {
        testBlock.find('#members').text(data.members)
        if (data.start){
            window.location.href = data.url
        }
      })
        })
    }, 2000)

}



$.fn.timer = function(){
    var timer = $(this)
    h = $(this).find('#h')
    m = $(this).find('#m')
    s = $(this).find('#s')

    question = parseInt(timer.attr('question'))
    cookieQuestion = parseInt($.cookie('current_question'))
    if (question == cookieQuestion){
        h.text($.cookie('h'))
        m.text($.cookie('m'))
        s.text($.cookie('s'))
    }else{
        $.cookie('current_question', question)
    }
   // console.log(parseInt(m.text()))
  var baseTime = new Date();
  baseTime.setMinutes(baseTime.getMinutes() - parseInt(m.text()))
  // Период сброса — 3 дня


  function update() {
    hours = parseInt(h.text())
    min = parseInt(m.text())
    sec = parseInt(s.text())
    if  (sec > 0){
        sec --;
    } else {
        if (min > 0) {
            min--;
            sec = 59;
        }else{
            if (hours > 0){
                hours --;
                min = 59;
                sec = 59;
            }else{

                $.ajax({
                    url: timer.attr('timeout-url'),
                    success:function (data) {
                         window.location.href = data.url
                    }
                })
            }
        }
    }
    if (hours < 10){
        hours = '0' + hours
    }
    if (min < 10){
        min = '0' + min
    }
    if (sec < 10){
        sec = '0' + sec
    }
    $.cookie('h', hours);
    $.cookie('m', min);
    $.cookie('s', sec);

    h.text(hours);
    m.text(min);
    s.text(sec);
  }
  var interval = setInterval(update, 1000);
}

$.fn.checkQuestion = function(){
    var testBlock = $(this);
    var url = testBlock.attr('check-url')
    setInterval(function () {
        $.ajax({
      url: url,
      success: (function (data) {
        console.log(data)
        if (!data.success){
            console.log(data.success)
            confirm(data.message);
            window.location.href = data.url
        }
      })
        })
    }, 2000)

}





$(document).ready(function(){
    if ($('#testWaiting').length > 0){
        $('#testWaiting').checkMembers();
    }
    if ($('#testProcess').length > 0) {
        $('#testProcess').checkQuestion()
    }
    if ($('#timer').length > 0) {
        $('#timer').timer()
    }
    $('form').on('submit', function(){
        $(this).find('button').attr('disabled', 'disableds')
    })
})