# Многопользовательская онлайн система тестирования


### Для уcтановки вам понадобится
- Git-client
- Python >= 3.6
- Django >= 2.0
- Postgresql >= 9.5
- Библиотека pyYAML
Установить ее можно через pip
```
pip3 install pyyaml
```

### Чтобы развернуть проект нужно выполнить:
```
git clone git@bitbucket.org:WikedWolf/testsystem.git
```
```
cd testsystem
```
Заменить в файле testsystem/testsystem настройки для БД
```
./manage.py migrate
```

Далее нужно применить фикстуры с начальными данными

```
./manage.py loaddata fixtures/fixture.yaml
```

Запустить проект локально можно командой
```
./manage.py runserver
```
## Чтобы начать тестирование, нужно в административной части в разделе "Тестирования" выделить нужное тестирование и применить действие